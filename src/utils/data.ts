import { FormDataType } from "../types";

export const initialData: FormDataType = {
  name: "",
  email: "",
  phoneNum: "",
  duration: "monthly",
  price: 0,
  type: "arcade",
  addons: [],
};
