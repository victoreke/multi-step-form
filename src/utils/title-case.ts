export function titleCase(title: string) {
  if (!title) return;
  return title.replace(/\w/, (firstWord) => firstWord.toUpperCase());
}
