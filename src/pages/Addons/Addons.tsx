import styles from "./addons.module.css";
import { addonsList } from "../../data/addons";
import { useFormStore } from "../../store/form";
import { AddonOptions } from "../../types";

export default function Addons() {
  const selectedAddons = useFormStore((state) => state.addons);
  const setAddons = useFormStore((state) => state.setAddons);
  const isChecked = (id: AddonOptions) =>
    selectedAddons.some((addon) => addon.id === id);
  const duration = useFormStore((state) => state.duration);

  return (
    <div className={styles.addons}>
      {addonsList.map((addon) => (
        <label
          htmlFor={addon.id}
          key={addon.id}
          className={styles.addon}
          style={
            isChecked(addon.id)
              ? { borderColor: "#02295a", backgroundColor: "#f8f9fe" }
              : {}
          }
        >
          <input
            type="checkbox"
            name={addon.name}
            id={addon.id}
            checked={isChecked(addon.id)}
            onChange={() =>
              setAddons({ id: addon.id, name: addon.name, price: addon.price })
            }
          />
          <span className={styles.checkmark}></span>
          <div className={styles.content}>
            <h3 className={styles.name}>{addon.name}</h3>
            <p className={styles.feature}>{addon.features}</p>
          </div>
          <p className={styles.price}>
            +&#36;{duration === "monthly" ? addon.price : addon.price * 10}
            <span>/{duration === "monthly" ? "mo" : "yr"}</span>
          </p>
        </label>
      ))}
    </div>
  );
}
