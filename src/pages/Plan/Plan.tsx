import React from "react";
import { plans } from "../../data/plans";
import styles from "./plan.module.css";
import { useFormStore } from "../../store/form";
import { titleCase } from "../../utils/title-case";

export default function Plans() {
  const duration = useFormStore((state) => state.duration);
  const setDuration = useFormStore((state) => state.setDuration);
  const planState = useFormStore((state) => state.plan);
  const setPlan = useFormStore((state) => state.setPlanType);
  const setPrice = useFormStore((state) => state.setPrice);

  return (
    <div>
      <div className={styles.plans}>
        {plans.map((plan) => (
          <React.Fragment key={plan.type}>
            <input
              type="radio"
              id={plan.type}
              name={plan.type}
              checked={plan.type === planState.type && true}
              onChange={() => {
                setPlan(plan.type);
                setPrice(plan.price);
              }}
            />
            <label key={plan.type} className={styles.plan} htmlFor={plan.type}>
              <plan.icon className={styles.icon} />
              <div>
                <h3 className={styles.planType}>{titleCase(plan.type)}</h3>
                <p className={styles.price}>
                  ${duration === "monthly" ? plan.price : plan.price * 10}
                  <span>/{duration === "monthly" ? "mo" : "yr"}</span>{" "}
                </p>
                {duration === "yearly" && (
                  <em className={styles.discount}>2 months free</em>
                )}
              </div>
            </label>
          </React.Fragment>
        ))}
      </div>
      <div className={styles.checkbox}>
        <button
          type="button"
          onClick={() => setDuration("monthly")}
          className={`${styles.duration}`}
          style={duration === "monthly" ? { color: "#02295a" } : {}}
        >
          Monthly
        </button>
        <input
          id="duration"
          name="duration"
          type="checkbox"
          checked={duration === "monthly" ? false : true}
          onChange={() =>
            setDuration(duration === "monthly" ? "yearly" : "monthly")
          }
        />
        <label htmlFor="duration"></label>
        <button
          type="button"
          onClick={() => setDuration("yearly")}
          className={styles.duration}
          style={duration === "yearly" ? { color: "#02295a" } : {}}
        >
          Yearly
        </button>
      </div>
    </div>
  );
}
