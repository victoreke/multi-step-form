import { FormDataType } from "../../types";
import { titleCase } from "../../utils/title-case";
import styles from "./summary.module.css";

export default function Summary({
  formData,
  setStep,
}: {
  formData: FormDataType;
  setStep: React.Dispatch<React.SetStateAction<number>>;
}) {
  const isMonthly = formData?.duration === "monthly";

  let totalAddons: number = 0;
  for (const { price } of formData.addons) {
    totalAddons += price;
  }

  return (
    <div className={styles.summary}>
      <div className={styles.header}>
        <div>
          <div>
            <h2 className={styles.plan}>
              {titleCase(formData?.type)} &#40;
              {titleCase(formData?.duration)}
              &#41;
            </h2>
            <button className={styles.btn} onClick={() => setStep(2)}>
              Change
            </button>
          </div>

          <p className={styles.price}>
            &#36;{isMonthly ? formData?.price : formData?.price}
            {isMonthly ? "/mo" : "/yr"}
          </p>
        </div>

        {formData.addons.length ? (
          <ul>
            {formData?.addons.map((addon) => (
              <li key={addon.id}>
                <span>{addon.name}</span>
                <p>
                  +&#36;{isMonthly ? addon.price : addon.price * 10}
                  {isMonthly ? "/mo" : "/yr"}
                </p>
              </li>
            ))}
          </ul>
        ) : (
          <p className={styles.emptyAddons}>No Addons Selected 😑</p>
        )}
      </div>

      <div className={styles.total}>
        <p>Total &#40;per {isMonthly ? "month" : "year"}&#41;</p>
        <em>
          +&#36;{(isMonthly ? totalAddons : totalAddons * 10) + formData.price}/
          {isMonthly ? "mo" : "yr"}
        </em>
      </div>
    </div>
  );
}
