import { useFormStore } from "../../store/form";
import { StatusOptions } from "../../types";
import styles from "./basic-info.module.css";

export default function BasicInfo({ status }: { status: StatusOptions }) {
  const personalInfo = useFormStore((state) => state.personalInfo);
  const setName = useFormStore((state) => state.setName);
  const setEmail = useFormStore((state) => state.setEmail);
  const setPhoneNum = useFormStore((state) => state.setPhoneNum);
  const { name, email, phoneNum } = personalInfo;

  return (
    <div className={styles.inputContainer}>
      <label htmlFor="name">
        <div className={styles.labelContainer}>
          <p className={styles.label}>Name</p>
          {status === "empty" && !name && (
            <span className={styles.error}>This field is required</span>
          )}
        </div>
        <input
          onChange={(e) => setName(e.target.value)}
          type="text"
          name="name"
          id="name"
          placeholder="e.g. Steven King"
          className={styles.input}
          style={
            status === "empty" && !name ? { border: "1px solid #ed3548" } : {}
          }
          value={name}
        />
      </label>

      <label htmlFor="email">
        <div className={styles.labelContainer}>
          <p className={styles.label}>Email Address</p>
          {status === "empty" && !email && (
            <span className={styles.error}>This field is required</span>
          )}
        </div>
        <input
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          name="email"
          id="email"
          placeholder="e.g. stephenking@lorem.com"
          className={styles.input}
          style={
            status === "empty" && !email ? { border: "1px solid #ed3548" } : {}
          }
          value={email}
        />
      </label>

      <label htmlFor="phone">
        <div className={styles.labelContainer}>
          <p className={styles.label}>Phone Number</p>
          {status === "empty" && !phoneNum && (
            <span className={styles.error}>This field is required</span>
          )}
        </div>
        <input
          onChange={(e) => {
            if (isNaN(+e.target.value)) {
              return;
            }
            setPhoneNum(e.target.value);
          }}
          type="text"
          name="phone"
          id="phone"
          placeholder="e.g. 0834 567 8955"
          className={styles.input}
          style={
            status === "empty" && !phoneNum
              ? { border: "1px solid #ed3548" }
              : {}
          }
          maxLength={11}
          value={phoneNum}
        />
      </label>
    </div>
  );
}
