import Header from "../../components/Header/Header";
import { SuccessIcon } from "../../components/icons";
import styles from "./success.module.css";

export default function Success() {
  return (
    <div className={styles.success}>
      <SuccessIcon className={styles.icon} />
      <Header
        title="Thank you!"
        description="Thanks for confirming your subscription! We hope you have fun using our platform. If you ever need support, please feel free to email us at support@loremgaming.com."
      />
    </div>
  );
}
