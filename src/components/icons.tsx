import { SVGAttributes } from "react";

export function SidebarDesktopPattern(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      width="275"
      height="100%"
      viewBox="0 0 275 568"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#clip0_3370_2)">
        <path
          d="M264.5 0H10.5C4.97715 0 0.5 4.47715 0.5 10V558C0.5 563.523 4.97715 568 10.5 568H264.5C270.023 568 274.5 563.523 274.5 558V10C274.5 4.47715 270.023 0 264.5 0Z"
          fill="#483EFF"
        />
        <mask
          id="mask0_3370_2"
          style={{ maskType: "alpha" }}
          maskUnits="userSpaceOnUse"
          x="0"
          y="0"
          width="275"
          height="100%"
        >
          <path
            d="M264.5 0H10.5C4.97715 0 0.5 4.47715 0.5 10V558C0.5 563.523 4.97715 568 10.5 568H264.5C270.023 568 274.5 563.523 274.5 558V10C274.5 4.47715 270.023 0 264.5 0Z"
            fill="white"
          />
        </mask>
        <g mask="url(#mask0_3370_2)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M-34.1921 543.102C3.7469 632.539 169.267 685.018 212.46 612.521C255.654 540.024 146.361 526.868 107.725 451.952C69.0899 377.036 39.0679 330.278 -16.7571 347.345C-72.5811 364.413 -72.1311 453.665 -34.1921 543.102Z"
            fill="#6259FF"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M233.595 601.155C294.274 572.877 326.434 457.629 275.47 429.627C224.505 401.624 218.073 477.206 167.411 505.614C116.749 534.022 85.271 555.821 98.367 593.855C111.463 631.889 172.916 629.433 233.595 601.155Z"
            fill="#F9818E"
          />
          <path
            d="M165.805 469.099L176.412 458.293M209.961 474.583L197.455 464.08M188.06 488.993L181.152 503.791"
            stroke="white"
            strokeWidth="5"
            strokeLinecap="round"
            strokeLinejoin="bevel"
          />
          <path
            d="M0.804932 546.891C37.8079 546.891 67.8049 516.894 67.8049 479.891C67.8049 442.888 37.8079 412.891 0.804932 412.891C-36.1981 412.891 -66.1951 442.888 -66.1951 479.891C-66.1951 516.894 -36.1981 546.891 0.804932 546.891Z"
            fill="#FFAF7E"
          />
        </g>
      </g>
      <defs>
        <clipPath id="clip0_3370_2">
          <rect
            width="274"
            height="100%"
            fill="white"
            transform="translate(0.5)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export function SidebarMobilePattern(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      width="100%"
      height="172"
      viewBox="0 0 100% 172"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#clip0_3372_25)">
        <rect width="100%" height="172" fill="#483EFF" />
        <g clipPath="url(#clip1_3372_25)">
          <path d="M0 0H390V172H0V0Z" fill="#483EFF" />
          <mask
            id="mask0_3372_25"
            style={{ maskType: "luminance" }}
            maskUnits="userSpaceOnUse"
            x="0"
            y="0"
            width="100%"
            height="172"
          >
            <path d="M0 0H390V172H0V0Z" fill="white" />
          </mask>
          <g mask="url(#mask0_3372_25)">
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M-74.3426 215.677C-17.6137 344.323 229.882 419.809 294.468 315.529C359.054 211.249 195.632 192.325 137.862 84.5663C80.0932 -23.1927 35.2027 -90.4487 -48.2698 -65.8997C-131.742 -41.3497 -131.07 87.0303 -74.3426 215.677Z"
              fill="#6259FF"
            />
            <path
              d="M-22.0117 221.129C33.3174 221.129 78.1705 177.981 78.1705 124.756C78.1705 71.5305 33.3174 28.3828 -22.0117 28.3828C-77.3408 28.3828 -122.194 71.5305 -122.194 124.756C-122.194 177.981 -77.3408 221.129 -22.0117 221.129Z"
              fill="#FFAF7E"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M326.405 299.191C417.207 258.501 465.333 92.6658 389.068 52.3708C312.802 12.0778 303.178 120.836 227.364 161.714C151.55 202.591 104.444 233.959 124.042 288.687C143.641 343.415 235.603 339.881 326.405 299.191Z"
              fill="#F9818E"
            />
            <path
              d="M224.959 109.17L240.833 93.6211M291.037 117.061L272.322 101.949M258.264 137.796L247.927 159.089"
              stroke="white"
              strokeWidth="5"
              strokeLinecap="round"
              strokeLinejoin="bevel"
            />
          </g>
        </g>
      </g>
      <defs>
        <clipPath id="clip0_3372_25">
          <rect width="100%" height="172" fill="white" />
        </clipPath>
        <clipPath id="clip1_3372_25">
          <rect width="100%" height="172" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
}

export function AdvancedIcon(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={40}
      height={40}
      {...props}
      viewBox="0 0 40 40"
    >
      <g fill="none" fillRule="evenodd">
        <circle cx="20" cy="20" r="20" fill="#F9818E" />
        <path
          fill="#FFF"
          fillRule="nonzero"
          d="M25.057 15H14.944C12.212 15 10 17.03 10 19.885c0 2.857 2.212 4.936 4.944 4.936h10.113c2.733 0 4.943-2.08 4.943-4.936S27.79 15 25.057 15ZM17.5 20.388c0 .12-.108.237-.234.237h-1.552v1.569c0 .126-.138.217-.259.217H14.5c-.118 0-.213-.086-.213-.203v-1.583h-1.569c-.126 0-.217-.139-.217-.26v-.956c0-.117.086-.213.202-.213h1.584v-1.554c0-.125.082-.231.203-.231h.989c.12 0 .236.108.236.234v1.551h1.555c.125 0 .231.083.231.204v.988Zm5.347.393a.862.862 0 0 1-.869-.855c0-.472.39-.856.869-.856.481 0 .87.384.87.856 0 .471-.389.855-.87.855Zm1.9 1.866a.86.86 0 0 1-.87-.852.86.86 0 0 1 .87-.855c.48 0 .87.38.87.855a.86.86 0 0 1-.87.852Zm0-3.736a.862.862 0 0 1-.87-.854c0-.472.39-.856.87-.856s.87.384.87.856a.862.862 0 0 1-.87.854Zm1.899 1.87a.862.862 0 0 1-.868-.855c0-.472.389-.856.868-.856s.868.384.868.856a.862.862 0 0 1-.868.855Z"
        />
      </g>
    </svg>
  );
}

export function ArcadeIcon(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={40}
      height={40}
      {...props}
      viewBox="0 0 40 40"
    >
      <g fill="none" fillRule="evenodd">
        <circle cx="20" cy="20" r="20" fill="#FFAF7E" />
        <path
          fill="#FFF"
          fillRule="nonzero"
          d="M24.995 18.005h-3.998v5.998h-2v-5.998H15a1 1 0 0 0-1 1V29a1 1 0 0 0 1 1h9.995a1 1 0 0 0 1-1v-9.995a1 1 0 0 0-1-1Zm-5.997 8.996h-2v-1.999h2v2Zm2-11.175a2.999 2.999 0 1 0-2 0v2.18h2v-2.18Z"
        />
      </g>
    </svg>
  );
}

export function ProIcon(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={40}
      height={40}
      {...props}
      viewBox="0 0 40 40"
    >
      <g fill="none" fillRule="evenodd">
        <circle cx="20" cy="20" r="20" fill="#483EFF" />
        <path
          fill="#FFF"
          fillRule="nonzero"
          d="M26.666 13H13.334A3.333 3.333 0 0 0 10 16.333v7.193a3.447 3.447 0 0 0 2.14 3.24c1.238.5 2.656.182 3.56-.8L18.52 23h2.96l2.82 2.966a3.2 3.2 0 0 0 3.56.8 3.447 3.447 0 0 0 2.14-3.24v-7.193A3.333 3.333 0 0 0 26.666 13Zm-9.333 6H16v1.333a.667.667 0 0 1-1.333 0V19h-1.333a.667.667 0 0 1 0-1.334h1.333v-1.333a.667.667 0 1 1 1.333 0v1.333h1.333a.667.667 0 1 1 0 1.334Zm7.333 2a2.667 2.667 0 1 1 0-5.333 2.667 2.667 0 0 1 0 5.333ZM26 18.333a1.333 1.333 0 1 1-2.667 0 1.333 1.333 0 0 1 2.667 0Z"
        />
      </g>
    </svg>
  );
}

export function CheckmarkIcon(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="9"
      viewBox="0 0 12 9"
      {...props}
    >
      <path
        fill="none"
        stroke="#FFF"
        strokeWidth="2"
        d="m1 4 3.433 3.433L10.866 1"
      />
    </svg>
  );
}

export function SuccessIcon(props: SVGAttributes<SVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={80}
      height={80}
      viewBox="0 0 80 80"
      {...props}
    >
      <g fill="none">
        <circle cx="40" cy="40" r="40" fill="#F9818E" />
        <path
          fill="#E96170"
          d="M48.464 79.167c.768-.15 1.53-.321 2.288-.515a40.04 40.04 0 0 0 3.794-1.266 40.043 40.043 0 0 0 3.657-1.63 40.046 40.046 0 0 0 12.463-9.898A40.063 40.063 0 0 0 78.3 51.89c.338-1.117.627-2.249.867-3.391L55.374 24.698a21.6 21.6 0 0 0-15.332-6.365 21.629 21.629 0 0 0-15.344 6.365c-8.486 8.489-8.486 22.205 0 30.694l23.766 23.775Z"
        />
        <path
          fill="#FFF"
          d="M40.003 18.333a21.58 21.58 0 0 1 15.31 6.351c8.471 8.471 8.471 22.158 0 30.63-8.47 8.47-22.156 8.47-30.627 0-8.47-8.472-8.47-22.159 0-30.63a21.594 21.594 0 0 1 15.317-6.35Zm9.865 15c-.316.028-.622.15-.872.344l-12.168 9.13-5.641-5.642c-1.224-1.275-3.63 1.132-2.356 2.356l6.663 6.663c.56.56 1.539.63 2.173.156l13.326-9.995c1.122-.816.43-2.993-.956-3.013a1.666 1.666 0 0 0-.17 0Z"
        />
      </g>
    </svg>
  );
}
