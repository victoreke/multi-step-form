import { SidebarDesktopPattern, SidebarMobilePattern } from "../icons";
import styles from "./sidebar.module.css";

export default function Sidebar({
  step,
  pages,
}: {
  step: number;
  pages: { name: string; id: number; title: string; description: string }[];
}) {
  return (
    <nav className={styles.sidebar}>
      <SidebarDesktopPattern className={styles.desktopBg} />
      <SidebarMobilePattern className={styles.mobileBg} />
      <ul className={styles.sidebarStep}>
        {pages.map((page, id) => (
          <li key={page.id}>
            <span
              className={`${styles.sidebarDot} ${
                step === page.id || step === 5 ? `${styles.activeDot}` : null
              }`}
            >
              {id + 1}
            </span>
            <div className={styles.sidebarDetails}>
              <h3>Step {id + 1}</h3>
              <em>{page.name}</em>
            </div>
          </li>
        ))}
      </ul>
    </nav>
  );
}
