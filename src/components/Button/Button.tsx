import styles from "./button.module.css";
import { CSSProperties, MouseEventHandler } from "react";

type ButtonType = {
  children: React.ReactNode;
  style?: CSSProperties;
  type?: "submit" | "reset" | "button";
  theme?: string;
  onHandleButton: MouseEventHandler<HTMLButtonElement>;
};

export default function Button({
  children,
  style,
  type,
  theme,
  onHandleButton,
}: ButtonType) {
  return (
    <button
      className={
        theme === "primary"
          ? `${styles.btn} ${styles.btnPrimary}`
          : `${styles.btn}`
      }
      style={style}
      type={type}
      onClick={onHandleButton}
    >
      {children}
    </button>
  );
}
