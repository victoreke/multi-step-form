import styles from "./header.module.css";

export default function Header({
  title,
  description,
}: {
  title: string;
  description: string;
}) {
  return (
    <header className={styles.formHeader}>
      <h1 className={styles.formTitle}>{title}</h1>
      <p className={styles.formDescription}>{description}</p>
    </header>
  );
}
