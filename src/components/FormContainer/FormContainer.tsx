import React from "react";
import styles from "./form-container.module.css";

export default function FormContainer({
  children,
  handleSubmit,
}: {
  children: React.ReactNode;
  handleSubmit: React.FormEventHandler<HTMLFormElement>;
}) {
  return (
    <form action="#" className={styles.form} onSubmit={handleSubmit}>
      {children}
    </form>
  );
}
