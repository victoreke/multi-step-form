import { create } from "zustand";
import { PlanOptions, AddonOptions, DurationOptions } from "../types";

interface AddonsType {
  id: AddonOptions;
  price: number;
  name: string;
}

interface StoreState {
  personalInfo: {
    name: string;
    email: string;
    phoneNum: string;
  };
  plan: {
    price: number;
    type: PlanOptions;
  };
  duration: DurationOptions;
  addons: AddonsType[];
}

interface StoreAction extends StoreState {
  setName: (name: StoreState["personalInfo"]["name"]) => void;
  setEmail: (email: StoreState["personalInfo"]["email"]) => void;
  setPhoneNum: (phone: StoreState["personalInfo"]["phoneNum"]) => void;
  setPlanType: (type: PlanOptions) => void;
  setPrice: (price: StoreState["plan"]["price"]) => void;
  setDuration: (duration: DurationOptions) => void;
  setAddons: (addon: AddonsType) => void;
}

const initialState: StoreState = {
  personalInfo: { name: "", email: "", phoneNum: "" },
  plan: { price: 9, type: "arcade" },
  duration: "monthly",
  addons: [
    {
      id: "online",
      price: 1,
      name: "Online service",
    },
    {
      id: "storage",
      price: 2,
      name: "Large storage",
    },
  ],
};

export const useFormStore = create<StoreState & StoreAction>((set, get) => ({
  ...initialState,
  setName: (name) =>
    set((state) => ({ personalInfo: { ...state.personalInfo, name } })),
  setEmail: (email) =>
    set((state) => ({ personalInfo: { ...state.personalInfo, email } })),
  setPhoneNum: (phone) =>
    set((state) => ({
      personalInfo: { ...state.personalInfo, phoneNum: phone },
    })),
  setPlanType: (type) => set((state) => ({ plan: { ...state.plan, type } })),
  setPrice: (price) => set((state) => ({ plan: { ...state.plan, price } })),
  setDuration: (duration) => set({ duration }),
  setAddons: (addon) => {
    const { addons } = get();
    const exists = addons.find((a) => a.id === addon.id);
    set({
      addons: exists
        ? addons.filter((a) => a.id !== addon.id)
        : [...addons, addon],
    });
  },
}));
