import { SVGAttributes } from "react";

export type PlanOptions = "arcade" | "advanced" | "pro";
export type AddonOptions = "online" | "storage" | "customize";
export type DurationOptions = "monthly" | "yearly";
export type StatusOptions = "idle" | "empty" | "summary";
interface AddonsType {
  id: AddonOptions;
  price: number;
  name: string;
}

export type PlanType = {
  type: PlanOptions;
  price: number;
  icon: (props: SVGAttributes<SVGElement>) => JSX.Element;
}[];

export type AddonType = {
  id: AddonOptions;
  name: string;
  features: string;
  price: number;
}[];

export type FormDataType = {
  name: string;
  email: string;
  phoneNum: string;
  duration: DurationOptions;
  price: number;
  type: PlanOptions;
  addons: AddonsType[];
};
