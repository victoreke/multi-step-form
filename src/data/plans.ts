import { AdvancedIcon, ArcadeIcon, ProIcon } from "../components/icons";
import { PlanType } from "../types";

export const plans: PlanType = [
  {
    type: "arcade",
    price: 9,
    icon: ArcadeIcon,
  },
  {
    type: "advanced",
    price: 12,
    icon: AdvancedIcon,
  },
  {
    type: "pro",
    price: 15,
    icon: ProIcon,
  },
];
