import { AddonType } from "../types";

export const addonsList: AddonType = [
  {
    id: "online",
    name: "Online service",
    features: "Access to multiplayer games",
    price: 1,
  },
  {
    id: "storage",
    name: "Large storage",
    features: "Extra 1TB of cloud save",
    price: 2,
  },
  {
    id: "customize",
    name: "Customize profile",
    features: "Custom theme on your profile",
    price: 2,
  },
];
