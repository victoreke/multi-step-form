import { useState } from "react";
import "./styles/app.css";
import Sidebar from "./components/Sidebar/Sidebar";
import FormContainer from "./components/FormContainer/FormContainer";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Success from "./pages/Success/Success";
import BasicInfo from "./pages/BasicInfo/BasicInfo";
import Plans from "./pages/Plan/Plan";
import Addons from "./pages/Addons/Addons";
import Summary from "./pages/Summary/Summary";
import { pages } from "./data/pages";
import { useFormStore } from "./store/form";
import { FormDataType, StatusOptions } from "./types";
import { initialData } from "./utils/data";

export default function App() {
  const [step, setStep] = useState(1);
  const personalInfo = useFormStore((state) => state.personalInfo);
  const duration = useFormStore((state) => state.duration);
  const plan = useFormStore((state) => state.plan);
  const addons = useFormStore((state) => state.addons);
  const [formData, setFormData] = useState<FormDataType>(initialData);
  const [status, setStatus] = useState<StatusOptions>("idle");

  const isEmpty =
    !personalInfo.name || !personalInfo.email || !personalInfo.phoneNum;

  function handleStep() {
    if (isEmpty) {
      setStatus("empty");
      return;
    }
    setStep((step) => (step <= 4 ? step + 1 : step));
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setFormData({
      ...personalInfo,
      duration,
      price: duration === "monthly" ? plan.price : plan.price * 10,
      type: plan.type,
      addons,
    });
  }

  return (
    <main className="container">
      <section className="form-container">
        <Sidebar pages={pages} step={step} />
        <FormContainer handleSubmit={handleSubmit}>
          {step <= 4 ? (
            <>
              <div>
                <Header
                  title={pages[step - 1].title}
                  description={pages[step - 1].description}
                />
                {step === 1 && <BasicInfo status={status} />}
                {step === 2 && <Plans />}
                {step === 3 && <Addons />}
                {step === 4 && (
                  <Summary formData={formData} setStep={setStep} />
                )}
              </div>
              <div className="btn-container">
                {step > 1 && (
                  <Button
                    onHandleButton={(e) => {
                      e.preventDefault();
                      setStep((step) => (step > 1 ? step - 1 : step));
                    }}
                  >
                    Go Back
                  </Button>
                )}
                <Button
                  style={step >= 4 ? { backgroundColor: "#473dff" } : {}}
                  theme="primary"
                  type={step === 4 ? "submit" : "button"}
                  onHandleButton={handleStep}
                >
                  {step < 4 ? "Next Step" : "Confirm"}
                </Button>
              </div>
            </>
          ) : (
            <Success />
          )}
        </FormContainer>
      </section>
    </main>
  );
}
